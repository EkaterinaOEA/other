#!/bin/sh
. /etc/selinux/config

if [ $(id -u) -ne 0 ]; then
    echo "Please switch to Root User"
    echo "type .su and press enter"
    echo "after run script again"
    exit
fi

function set_status_in_config() {
    sed -i "s/\(^SELINUX=\).*/\1$1/"  /etc/selinux/config
    
}

function enable_selinux() {
    config_command=""
    if [ "$state" == "Enforcing" ]; then
        config_command="disabled"

    elif
        [ "$state" == "Permissive" ]
    then
        config_command="disabled"

    elif [ "$state" == "Disabled" ]; then
        config_command="permissive"

    else
        echo "unsupported command"
        exit
    fi

    clear
    echo "[*] changed in config SELINUX=$config_command"
    echo

    set_status_in_config $config_command

    read -p 'You must restart your computer to apply these changes. Restart now? [y/N]  ' answer

    if [[ $answer == y* ]]; then
       reboot 
        exit
    else
        exit
    fi
}

function enable_mode_selinux() {
    clear

    if [ "$state" == "Permissive" ]; then
        setenforce 1
        echo "setenforce 1"
    elif [ "$state" == "Enforcing" ]; then
        setenforce 0
        echo "setenforce 0"
    else
        echo "unsupported command"
        exit
    fi

    echo
    echo "DONE"
    read -e -p 'Please, enter for continue or enter 1 for exit:   ' answer

    if [[ $answer == 1 ]]; then
        exit
    else
        check_status
    fi

}

function check_status() {
    state=$(getenforce)
    display_menu
}

function describe_status() {
     
    if [ "$2" == "Disabled" ] || [ "$2" == "disabled" ]; then
        echo "$1: disabled"

    elif [ "$2" == "Enforcing" ] || [ "$2" == "enforcing" ]; then
        echo "$1: enabled/activate"

    elif [ "$2" == "Permissive" ] || [ "$2" == "permissive" ]; then
        echo "$1: enabled/deactivate(only logging) "
    else
        echo "unsupported command"
        exit
    fi
}

function display_menu() {
    clear
    echo "selinux_status"
    describe_status "right now: " "$state"
    describe_status "in config: " "$SELINUX"
   
    echo "-------------------------------------"
    echo "0 = check status"
    echo

    if [ "$state" == "Disabled" ]; then
        echo "1 = enable (only logging)"

    elif [ "$state" == "Enforcing" ]; then
        echo "1 = disable"

    elif [ "$state" == "Permissive" ]; then
        echo "1 = disable"
    else
        echo "unsupported command"
        exit
    fi

    if [ "$state" == "Permissive" ]; then
        echo "2 = activate"

    elif [ "$state" == "Enforcing" ]; then
        echo "2 = deactivate (only logging)"
    fi

    echo
    echo "5 = exit"
    echo "-------------------------------------"
    echo

    read -p "enter command: " command

    case $command in
    0) check_status ;;
    1) enable_selinux ;;
    2) enable_mode_selinux ;;
    5) exit ;;
    esac
}

state=$(getenforce)
display_menu